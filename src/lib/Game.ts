import { config } from "../config/constants";

export class Game {
  time: { start: number; elapsed: number; step: number };
  ctx: CanvasRenderingContext2D;

  constructor(ctx: CanvasRenderingContext2D) {
    this.time = { start: 0, elapsed: 0, step: 1000 };
    this.ctx = ctx;
  }

  init() {
    this.ctx.canvas.width = config.WIDTH;
    this.ctx.canvas.height = config.HEIGHT;

    this.run();
  }

  run = (now = 0) => {
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

    this.ctx.font = "30px Arial";
    this.ctx.fillText("Animation timer: " + now, 100, 100);

    // animate
    requestAnimationFrame(this.run);
  };
}
