import { Game } from "./lib/Game";

// setup canvas element
const canvas: HTMLCanvasElement = document.querySelector("canvas#game");
const ctx: CanvasRenderingContext2D = canvas.getContext("2d");

// Init and run game
const game = new Game(ctx);
game.init();
